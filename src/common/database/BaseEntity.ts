import {
  PrimaryGeneratedColumn,
  // UpdateDateColumn,
  // CreateDateColumn,
  BaseEntity as TypeormBaseEntity,
} from 'typeorm';

export abstract class BaseEntity extends TypeormBaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  /*
  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  createDateTime: Date;

  @UpdateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  lastChangedDateTime: Date;
   */
}
