import { Module } from '@nestjs/common';
import { AppConfigModule } from './config/app/configuration.module';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppConfigService } from './config/app/configuration.service';
import { ORMConfigModule } from './config/db/configuration.module';
import { ORMConfigService } from './config/db/configuration.service';
import { SingesModule } from './modules/singes/singes.modules';
import { AppController } from "./app.controller";

@Module({
  imports: [
    ConfigModule.forRoot(),
    AppConfigModule,
    TypeOrmModule.forRootAsync({
      imports: [AppConfigModule, ORMConfigModule],
      inject: [AppConfigService, ORMConfigService],
      useFactory: (
        appConfigService: AppConfigService,
        ormConfigService: ORMConfigService,
      ) => ({
        // type: ormConfigService.type,
        type: 'mysql',
        host: ormConfigService.host,
        port: ormConfigService.port,
        username: ormConfigService.username,
        password: ormConfigService.password,
        database: ormConfigService.database,
        entities: [__dirname + '/**/*.entity{.ts,.js}'],
        synchronize: !appConfigService.isProduction,
        entityPrefix: ormConfigService.entityPrefix,
      }),
    }),
    SingesModule,
  ],
  controllers: [AppController],
})
export class AppModule {}
