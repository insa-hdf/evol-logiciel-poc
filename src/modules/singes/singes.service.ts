import { Injectable } from '@nestjs/common';
import { Singe } from './singe.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ISinge } from './interfaces/singe.interface';

@Injectable()
export class SingesService {
  constructor(
    @InjectRepository(Singe)
    private singesRepository: Repository<Singe>,
  ) {}

  create(singe: ISinge): Promise<ISinge> {
    return this.singesRepository.save(singe);
  }

  findAll(): Promise<ISinge[]> {
    return this.singesRepository.find();
  }

  findOne(id: number): Promise<ISinge> {
    return this.singesRepository.findOne(id);
  }

  async update(id: number, singe: ISinge): Promise<ISinge> {
    return this.singesRepository.save({ ...singe, id });
  }

  async remove(id: number): Promise<void> {
    await this.singesRepository.delete(id);
  }
}
