import { IsNumber, IsString } from 'class-validator';

export class RequestUpdateSingeDto {
  @IsString()
  readonly name: string;

  @IsNumber()
  readonly age: number;

  @IsNumber()
  readonly nbBanane: number;
}
