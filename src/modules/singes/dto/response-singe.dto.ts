import { ISinge } from '../interfaces/singe.interface';

export class ResponseSingeDto {
  constructor(singe: ISinge) {
    this.id = singe.id;
    this.name = singe.name;
    this.age = singe.age;
    this.nbBanane = singe.nbBanane;
  }

  id: number;
  name: string;
  age: number;
  nbBanane: number;
}
