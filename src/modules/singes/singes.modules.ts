import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SingesController } from './singes.controller';
import { SingesService } from './singes.service';
import { Singe } from './singe.entity';

@Module({
  controllers: [SingesController],
  providers: [SingesService, Singe],
  imports: [TypeOrmModule.forFeature([Singe])],
  exports: [SingesService],
})
export class SingesModule {}
