import { ISinge } from './interfaces/singe.interface';
import { BaseEntity } from '../../common/database/BaseEntity';
import { Column, Entity } from 'typeorm';

@Entity({ name: 'singe' })
export class Singe extends BaseEntity implements ISinge {
  @Column({ type: 'varchar' })
  name: string;

  @Column({ type: 'int' })
  age: number;

  @Column({ type: 'int', default: 0 })
  nbBanane: number;
}
