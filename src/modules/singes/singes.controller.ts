import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  ParseIntPipe,
  Post,
  Put,
  UseInterceptors,
} from '@nestjs/common';
import { SingesService } from './singes.service';
import { RequestCreateSingeDto } from './dto/request-create-singe.dto';
import { ISinge } from './interfaces/singe.interface';
import { ResponseSingeDto } from './dto/response-singe.dto';
import { RequestUpdateSingeDto } from './dto/request-update-singe.dto';

@UseInterceptors(ClassSerializerInterceptor)
@Controller('singes')
export class SingesController {
  constructor(private singesService: SingesService) {}

  @Post()
  async create(
    @Body() createSingeDto: RequestCreateSingeDto,
  ): Promise<ResponseSingeDto> {
    const singe: ISinge = await this.singesService.create({
      name: createSingeDto.name,
      age: createSingeDto.age,
      nbBanane: 0,
    });
    return new ResponseSingeDto(singe);
  }

  @Get()
  async findAll(): Promise<ResponseSingeDto[]> {
    const singes: ISinge[] = await this.singesService.findAll();
    return singes.map((singe) => new ResponseSingeDto(singe));
  }

  @Get(':id')
  async findOne(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<ResponseSingeDto> {
    const singe: ISinge = await this.singesService.findOne(id);
    return new ResponseSingeDto(singe);
  }

  @Put(':id')
  async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateSingeDto: RequestUpdateSingeDto,
  ): Promise<ResponseSingeDto> {
    const singe: ISinge = await this.singesService.update(id, updateSingeDto);
    return new ResponseSingeDto(singe);
  }

  @Delete(':id')
  @HttpCode(204)
  async remove(@Param('id', ParseIntPipe) id: number) {
    await this.singesService.remove(id);
  }
}
