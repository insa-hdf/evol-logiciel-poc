import * as Joi from '@hapi/joi';
import { Module } from '@nestjs/common';
import configuration from './configuration';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ORMConfigService } from './configuration.service';

/**
 * Import and provide postgres configuration related classes.
 *
 * @module
 */
@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
      validationSchema: Joi.object({
        ORM_TYPE: Joi.string().default('mysql'),
        ORM_HOST: Joi.string().default('localhost'),
        ORM_PORT: Joi.number().default(5432),
        ORM_USER: Joi.string().default('root'),
        ORM_PASSWORD: Joi.string().default('root'),
        ORM_DATABASE: Joi.string().default('DB'),
        ORM_ENTITY_PREFIX: Joi.string().default(''),
      }),
    }),
  ],
  providers: [ConfigService, ORMConfigService],
  exports: [ConfigService, ORMConfigService],
})
export class ORMConfigModule {}
