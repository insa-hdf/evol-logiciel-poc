import { registerAs } from '@nestjs/config';

export default registerAs('orm', () => ({
  type: process.env.ORM_TYPE,
  host: process.env.ORM_HOST,
  port: process.env.ORM_PORT,
  username: process.env.ORM_USER,
  password: process.env.ORM_PASSWORD,
  database: process.env.ORM_DATABASE,
  entityPrefix: process.env.ORM_ENTITY_PREFIX,
}));
