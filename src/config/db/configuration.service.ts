import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

/**
 * Service dealing with postgres config based operations.
 *
 * @class
 */
@Injectable()
export class ORMConfigService {
  constructor(private configService: ConfigService) {}

  get type(): string {
    return this.configService.get<string>('orm.type');
  }
  get host(): string {
    return this.configService.get<string>('orm.host');
  }
  get port(): number {
    return this.configService.get<number>('orm.port');
  }
  get username(): string {
    return this.configService.get<string>('orm.username');
  }
  get password(): string {
    return this.configService.get<string>('orm.password');
  }
  get database(): string {
    return this.configService.get<string>('orm.database');
  }
  get entityPrefix(): string {
    return this.configService.get<string>('orm.entityPrefix');
  }
}
