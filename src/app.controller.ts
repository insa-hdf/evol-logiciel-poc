import { Get, Controller, Render, ParseIntPipe, Param } from '@nestjs/common';

@Controller()
export class AppController {
  @Get('/static')
  @Render('static')
  pageStatic() {
    return;
  }

  @Get('/calcul/:max')
  @Render('calcul')
  pageCalcul(@Param('max', ParseIntPipe) max: number) {
    const L = [];
    const primes = [];
    let i, j;

    for (i = 2; i <= max; ++i) {
      if (!L[i]) {
        primes.push(i);
        for (j = i << 1; j <= max; j += i) {
          L[j] = true;
        }
      }
    }
    return { primes };
  }
}
